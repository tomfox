#!/usr/bin/env python

import sys, dbus, gobject, dbus.glib

# Get arguments
title = sys.argv[1]
note = sys.argv[2]
url = sys.argv[3]
display = sys.argv[4]
notebook = sys.argv[5]
appendnote =  sys.argv[6]


# Format URL for XML
if len(url) > 0: url = "<italic><size:small>Source: <link:url>" + url + "</link:url></size:small></italic>\n\n"


# Connect to Tomboy DBUS
bus = dbus.SessionBus()
obj = bus.get_object("org.gnome.Tomboy", "/org/gnome/Tomboy/RemoteControl")
tomboy = dbus.Interface(obj, "org.gnome.Tomboy.RemoteControl")


# Create/Append Note
if tomboy.NoteExists(tomboy.FindNote(title)) == 1 and appendnote == "true": 
	uri = tomboy.FindNote(title)
	currentcontents = tomboy.GetNoteContentsXml(uri)
	currentcontents = currentcontents.encode('utf-8')
	separator = "<strikethrough>"
	for i in range(1, 70): separator += " "
	separator += "</strikethrough>"
	tomboy.SetNoteContentsXml(uri, "<note-content>" + currentcontents + separator + "\n\n" + note + url + "</note-content>")
else:
	# Check if title exists, append #1, #2, #3... to the end of the note title
	if tomboy.NoteExists(tomboy.FindNote(title)) == 1:
		notedupe = int(1)
		while tomboy.NoteExists(tomboy.FindNote(title + " #" + str(notedupe))) == 1:
			notedupe = notedupe + 1
		if notedupe > 0: title = title + " #" + str(notedupe)

	# Create the note and set the contents
	uri = tomboy.CreateNamedNote(title)
	tomboy.SetNoteContentsXml(uri, "<note-content>" + title + "\n\n" + note + url + "</note-content>")

	# Assign note to the default notebook
	if len(notebook) > 0: tomboy.AddTagToNote(uri, "system:notebook:" + notebook)


# Display Note if enabled
if display == "true": tomboy.DisplayNote(uri)


