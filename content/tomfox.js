if(!org) var org={};
if(!org.gnome) org.gnome={};
if(!org.gnome.tomboy) org.gnome.tomboy={};
if(!org.gnome.tomboy.ffext) org.gnome.tomboy.ffext={};


org.gnome.tomboy.ffext = {


	createnote: function(e){

		// Locale
		tb_strings = document.getElementById("tomfoxbundle");

		// Get Preferences
		var prefs = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch);

		with(prefs){

			var bDisplayNote;
			if (getPrefType("extensions.tomfox.bDisplayNote") == PREF_BOOL){
			  bDisplayNote = getBoolPref("extensions.tomfox.bDisplayNote");
			}
		
			var bAddUrl;
			if (getPrefType("extensions.tomfox.bAddUrl") == PREF_BOOL){
			  bAddUrl = getBoolPref("extensions.tomfox.bAddUrl");
			}

			var bPromptForTitle;
			if (getPrefType("extensions.tomfox.bPromptForTitle") == PREF_BOOL){
			  bPromptForTitle = getBoolPref("extensions.tomfox.bPromptForTitle");
			}

			var bAppend;
			if (getPrefType("extensions.tomfox.bNoteAppend") == PREF_BOOL){
			  bAppend = getBoolPref("extensions.tomfox.bNoteAppend");
			}

			var sDefaultNotebook = this.trim(getCharPref("extensions.tomfox.sDefaultNotebook"));			

		}


		// Get Note Title
		if (e.shiftKey==true) {bPromptForTitle=true};
		var tbtitle = this.PageTitle();
		if (bPromptForTitle==true) {tbtitle = window.prompt(tb_strings.getString("notetitleprompt"),tbtitle,"Tomfox")};
		if(tbtitle=="")	{tbtitle = "Untitled"};
		
		
		// Limit the size of the note title
		tbtitle = tbtitle.substring(0,100);

				
		// Set Note Contents
		var tbnote = this.SelectedText();

		
		// Get URL
		var tbURL = ""
		if (bAddUrl==true) {tbURL = this.CurrentURL()};

			
		// Replace XML Characters
		tbtitle = String(this.ReplaceXMLchars(tbtitle));
		tbnote = String(this.ReplaceXMLchars(tbnote));
		tbURL = String(this.ReplaceXMLchars(tbURL));


		// Convert Unicode
		tbtitle = this.ConvertToUTF8(tbtitle);
		tbnote = this.ConvertToUTF8(tbnote);
		tbURL = this.ConvertToUTF8(tbURL);
		sDefaultNotebook = this.ConvertToUTF8(sDefaultNotebook);


		// Run tomboy dbus script                               
		var file = Components.classes["@mozilla.org/extensions/manager;1"]
			.getService(Components.interfaces.nsIExtensionManager)
			.getInstallLocation("tomfox@harrycoal.co.uk")
			.getItemLocation("tomfox@harrycoal.co.uk");
		file.append("tfdbus.py");
		if (file.exists()) {
			var process = Components.classes['@mozilla.org/process/util;1']
				.getService(Components.interfaces.nsIProcess);	

			try 
			  {
			  process.init(file);
			  }
			catch(err)
			  {
			  dump(err);
			  }


			var arguments= [tbtitle, tbnote, tbURL, bDisplayNote, sDefaultNotebook, bAppend] ; 
			process.run(false, arguments, arguments.length);
		}
		else {
			alert("Error: Cannot find Tomfox DBus script: tfdbus.py");
		}

	
	},


	ReplaceXMLchars: function(txt){
		// Replace reserved XML characters
		txt = txt.replace(new RegExp("&","g"), "\&amp;");
		txt = txt.replace(new RegExp("<","g"), "\&lt;");
		txt = txt.replace(new RegExp(">","g"), "\&gt;");
		txt = txt.replace(new RegExp("−","g"), "\&#45;");
		txt = txt.replace(new RegExp("%","g"), "\&#37;");
		return String(txt);		
	},
	
  
	SelectedText: function(){
		var focusedWindow = document.commandDispatcher.focusedWindow;
		var selection = focusedWindow.getSelection();
		seltext = "";
		for(var i = 0; i < selection.rangeCount; i++) {
			seltext = seltext + selection.getRangeAt(i) + "\n\n";
		}
		return seltext;
	},
	
	
	ConvertToUTF8: function(value){
		const conv = Components.classes["@mozilla.org/intl/scriptableunicodeconverter"].createInstance(Components.interfaces.nsIScriptableUnicodeConverter);
		conv.charset = "UTF-8";
		return conv.ConvertFromUnicode(value) + conv.Finish();		
	},
  
  
  	CurrentURL: function(){
	    var aDocShell = document.getElementById("content").webNavigation;
	    var url = aDocShell.currentURI.spec;
	    var title = null;
	    return url;	
	},
  
  
   	PageTitle: function(){
	    var aDocShell = document.getElementById("content").webNavigation;
	    var url = aDocShell.currentURI.spec;
	    var title = null;
	    try {
	      title = aDocShell.document.title;
	    }catch (e) {
	      title = url;
	    }
	    return title;		
	}, 
  
  
  	trim: function(stringToTrim){
		return stringToTrim.replace(/^\s+|\s+$/g,"");
	}
 
  
}



